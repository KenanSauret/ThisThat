package com.thisthat.thisthat.utils;

import android.app.Application;

import com.google.firebase.FirebaseApp;

/**
 * Created by Jean on 12/03/2018.
 */

public class ThisThatApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
    }
}