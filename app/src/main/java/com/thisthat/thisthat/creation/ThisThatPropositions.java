package com.thisthat.thisthat.creation;

import android.util.Pair;

import java.util.ArrayList;

/**
 * Created by kenan on 23/03/18.
 */

public class ThisThatPropositions {

    private ArrayList<Pair<String, String>> propositions;

    public ThisThatPropositions() {

    }

    public ThisThatPropositions(ArrayList<Pair<String, String>> propositions) {
        this.propositions = propositions;
    }

    public ArrayList<Pair<String, String>> getPropositions() {
        return propositions;
    }

    public void setPropositions(ArrayList<Pair<String, String>> propositions) {
        this.propositions = propositions;
    }

    @Override
    public String toString() {
        return "ThisThatPropositions{" +
                "propositions=" + propositions.toString() +
                '}';
    }
}
