package com.thisthat.thisthat.activities;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;

import com.thisthat.thisthat.fragments.CameraFragment;
import com.thisthat.thisthat.R;
import com.thisthat.thisthat.fragments.ThisThatFragment;
import com.thisthat.thisthat.creation.ThisThatPropositions;

import java.util.ArrayList;

public class ThisThatCreationActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "TTCreationActivity";
    private Button mButtonVideo;
    private Button mButtonStartThisThat;

    private CameraFragment cameraFragment;
    private ThisThatFragment thisthatFragment;

    private ThisThatPropositions propositions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cameraFragment = CameraFragment.newInstance();
        thisthatFragment = ThisThatFragment.newInstance();
        thisthatFragment.resetDisplayCount();

        cameraFragment.setThisthatFragment(thisthatFragment);
        thisthatFragment.setCameraFragment(cameraFragment);

        ArrayList<Pair<String,String>> listProps = new ArrayList<>();
        Pair<String,String> pair1 = new Pair<>("OM", "PSG");
        Pair<String,String> pair2 = new Pair<>("OUI", "NON");
        Pair<String,String> pair3 = new Pair<>("GAUCHE", "DROITE");
        Pair<String,String> pair4 = new Pair<>("ETE", "HIVER");
        listProps.add(pair1);
        listProps.add(pair2);
        listProps.add(pair3);
        listProps.add(pair4);

        propositions = new ThisThatPropositions(listProps);
        thisthatFragment.setPropositions(propositions);

        setContentView(R.layout.activity_thisthat_creation);
        if (null == savedInstanceState) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, cameraFragment
                    )
                    .commit();
        }

        mButtonVideo = (Button) findViewById(R.id.video);
        mButtonVideo.setOnClickListener(this);
        mButtonStartThisThat = (Button) findViewById(R.id.start_thisthat);
        mButtonStartThisThat.setOnClickListener(this);
        findViewById(R.id.info).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.video: {
//                if (mIsRecordingVideo) {
//                    stopRecordingVideo();
//                } else {
//                    startRecordingVideo();
//                }
//                break;
//            }
            case R.id.info: {
                new AlertDialog.Builder(this)
                        .setMessage(R.string.intro_message)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
                break;
            }
            case R.id.start_thisthat: {
                Log.d(this.toString(), "Switch to ThisThat proposition");
                this.getFragmentManager().beginTransaction()
                        .hide(cameraFragment)
                        .commit();

                thisthatFragment.setStringThis("OM");
                thisthatFragment.setStringThat("PSG");

                this.getFragmentManager().beginTransaction()
                        .replace(R.id.constraintLayoutDisplay, cameraFragment.getThisthatFragment())
                        .commit();
                break;
            }
        }
    }


}
